﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SQLiteWP81.Resources;

namespace SQLiteWP81
{
    public partial class MainPage : PhoneApplicationPage
    {
        WorkerDB _workerDb = new WorkerDB();
        
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            // create db
            _workerDb.DbCreator();
            //Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void Settings_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
        }

        private void About_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            //_workerDb.GetTransactionList();

            MessageBox.Show("Обновлено!");
        }

        private void addTrasaction_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/TransactionPage.xaml", UriKind.Relative));
        }

        private void settings_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
        }
    }
}