﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Channels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SQLiteWP81
{
    public partial class AccountsPage : PhoneApplicationPage
    {
        WorkerDB _query = new WorkerDB();
        public AccountsPage()
        {
            InitializeComponent();
            ShowAccounts();
        }

        public void ShowAccounts()
        {
            List<WorkerDB.Account> list = _query.ShowAccounts();
            //List<string> liststr = new List<string>();
            List<AcHelper> acHelpers = new List<AcHelper>();
            foreach (var account in list)
            {
                //liststr.Add("Счет: " + account.Name + ".   Баланс: " + account.Amount + ".");
                acHelpers.Add(new AcHelper{Amount = "Баланс: " + account.Amount, Name = account.Name});
            }
            AccountsListBox.ItemsSource = acHelpers;
        }

        private void addAccount_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddAccountPage.xaml", UriKind.Relative));
        }

        private void TextBlock_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Border border = sender as Border;
            ContextMenu contextMenu = ContextMenuService.GetContextMenu(border);
            if (contextMenu.Parent == null)
            {
                contextMenu.IsOpen = true;
            }
            MessageBox.Show("Hold");
        }
    }

    public class AcHelper
    {
        public string Name { get; set; }
        public string Amount { get; set; }
    }
}