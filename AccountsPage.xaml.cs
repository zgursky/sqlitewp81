﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.Channels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SQLiteWP81
{
    public partial class AccountsPage : PhoneApplicationPage
    {
        WorkerDB _query = new WorkerDB();
        public AccountsPage()
        {
            InitializeComponent();
            ShowAccounts();
        }

        public void ShowAccounts()
        {
            List<WorkerDB.Account> list = _query.ShowAccounts();
            //List<string> liststr = new List<string>();
            List<AccountHelper> acHelpers = new List<AccountHelper>();
            foreach (var account in list)
            {
                //liststr.Add("Счет: " + account.Name + ".   Баланс: " + account.Amount + ".");
                acHelpers.Add(new AccountHelper { Amount = "Баланс: " + account.Amount, Name = account.Name });
            }
            AccountsListBox.ItemsSource = acHelpers;
        }

        private void addAccount_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddAccountPage.xaml", UriKind.Relative));
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Edit");
        }
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var name = sender as MenuItem;
            var str = name.CommandParameter;
            var accountName = ((AccountHelper)(str)).Name;
            _query.DeleteAccount(accountName);
            MessageBox.Show("Удален cчет: " + accountName + ".");
            ShowAccounts();
        }

        private void BackKeyPress(object sender, CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
        }
    }

    class GetAccountName
    {
        
    }

    public class AccountHelper
    {
        public string Name { get; set; }
        public string Amount { get; set; }
    }
}