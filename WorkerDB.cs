﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using SQLite;
using Windows.Storage;
using System.IO;

namespace SQLiteWP81
{
    class WorkerDB
    {
        public static string DB_PATH = Path.Combine(Path.Combine(ApplicationData.Current.LocalFolder.Path, "myfin.db"));
        public static SQLiteConnection dbConnection;

        public void DbCreator()
        {
            //проверяем наличие БД. если отсутствует - создаем таблицы и заполняем начальными значениями
            if (FileExists("myfin.db"))
            {

                dbConnection = new SQLiteConnection(DB_PATH);

                dbConnection.CreateTable<Currency>();
                dbConnection.CreateTable<CurrencyDefault>();
                dbConnection.CreateTable<User>();
                dbConnection.CreateTable<CategoryTransaction>();
                dbConnection.CreateTable<Account>();
                dbConnection.CreateTable<Transaction>();

                //dbConnection.RunInTransaction(() =>
                //{
                //    dbConnection.Insert(new Account() { Name = "first", Amount = "1000", CurrencyId = "1", Id = 1 });
                //});
            }
        }

        private bool FileExists(string fileName)
        {
            var result = false;
            try
            {
                var strb = new StringBuilder();
                var str = strb.Append(DB_PATH, (DB_PATH.Length - 8), 8).ToString();
                if (str == fileName)
                {
                    result = true;
                }
            }
            catch
            {
            }
            return result;
        }

        #region Currency

        public void AddCurrencyDefault()
        {
            var currencyDefaults = GetCurrencyDefault();
            if (currencyDefaults.Count == 0)
            {
                dbConnection.RunInTransaction(() => dbConnection.Insert(new CurrencyDefault() { Name = "USD", Description = "Dollar", Symbol = "$", Course = "", Goad = "" }));
                dbConnection.RunInTransaction(() => dbConnection.Insert(new CurrencyDefault() { Name = "EUR", Description = "Euro", Symbol = "€", Course = "", Goad = "" }));
                dbConnection.RunInTransaction(() => dbConnection.Insert(new CurrencyDefault() { Name = "UAH", Description = "Hryvnia", Symbol = "₴", Course = "", Goad = "" }));
                dbConnection.RunInTransaction(() => dbConnection.Insert(new CurrencyDefault() { Name = "RUB", Description = "Ruble", Symbol = "руб.", Course = "", Goad = "" }));
                dbConnection.RunInTransaction(() => dbConnection.Insert(new CurrencyDefault() { Name = "JPY", Description = "Yen", Symbol = "¥", Course = "", Goad = "" }));
                dbConnection.RunInTransaction(() => dbConnection.Insert(new CurrencyDefault() { Name = "GBP", Description = "Pound Sterling", Symbol = "£", Course = "", Goad = "" }));
            }
        }

        public List<CurrencyDefault> GetCurrencyDefault()
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("select * from CurrencyDefault;");
            sqLiteCommand.CommandText = sql;
            List<CurrencyDefault> currencyDefaults = sqLiteCommand.ExecuteQuery<CurrencyDefault>();
            return currencyDefaults;
        }
        public void AddCurrency(string name, string description, string symbol, string course, string goad)
        {
            dbConnection.RunInTransaction(() => dbConnection.Insert(new Currency()
            {
                Name = name,
                Description = description,
                Symbol = symbol,
                Course = course,
                Goad = goad
            }));
        }
        public string GetCurrencyCourse(string Name)
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("select Course from Currency where Name = '{0}';", Name);
            sqLiteCommand.CommandText = sql;
            var strResult = "";
            var resulr = sqLiteCommand.ExecuteQuery<Currency>();
            foreach (var s in resulr)
            {
                strResult = s.Course;
            }

            return strResult;
        }
        public List<string> GetCurencyNameList()
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("select * from Currency;");
            sqLiteCommand.CommandText = sql;
            var list = new List<string>();
            var resulr = sqLiteCommand.ExecuteQuery<Currency>();
            foreach (var c in resulr)
            {
                if (list.All(x => x != c.Name))
                {
                    list.Add(c.Name);
                }
            }
            return list;
        }
        public void SetCourseCurrency(string Name, string Course, string Symbol)
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("INSERT OR REPLACE INTO Currency (Name,Course,Symbol) VALUES ('{0}', '{1}', '{2}');", Name, Course, Symbol);
            sqLiteCommand.CommandText = sql;
            sqLiteCommand.ExecuteNonQuery();
        }
        public List<Currency> ShowCurrency()
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("select * from Currency;");
            sqLiteCommand.CommandText = sql;
            var resulr = sqLiteCommand.ExecuteQuery<Currency>();
            return resulr;
        }
        public void DeleteCurrency(string currencyName)
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("delete from Currency where name = '{0}';", currencyName);
            sqLiteCommand.CommandText = sql;
            sqLiteCommand.ExecuteNonQuery();
        }
        public void DeleteAllCurrency()
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("delete from Currency;");
            sqLiteCommand.CommandText = sql;
            sqLiteCommand.ExecuteNonQuery();
        }
        #endregion

        #region Account

        public void SetAccount(string Name, string Amount, string CurrencyId)
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("INSERT OR REPLACE INTO Account (Name,Amount,CurrencyId) VALUES ('{0}', '{1}', '{2}');", Name, Amount, CurrencyId);
            sqLiteCommand.CommandText = sql;
            sqLiteCommand.ExecuteNonQuery();
        }
        public List<string> GetAccountName()
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("select Name from Account;");
            sqLiteCommand.CommandText = sql;
            var list = new List<string>();
            var resulr = sqLiteCommand.ExecuteQuery<Account>();
            foreach (var c in resulr)
            {
                list.Add(c.Name);
            }
            return list;
        }
        public List<Account> ShowAccounts()
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("select * from Account;");
            sqLiteCommand.CommandText = sql;
            var resulr = sqLiteCommand.ExecuteQuery<Account>();
            return resulr;
        }
        public void DeleteAccount(string accountName)
        {
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("delete from Account where name = '{0}';", accountName);
            sqLiteCommand.CommandText = sql;
            sqLiteCommand.ExecuteNonQuery();
        }
        #endregion

        #region Transaction
        class Transact
        {
            public string nameTransact;
            public string sumTransact;
        }

        public List<string> GetTransactionList()
        {
            var transact = new Transact();
            var sqLiteCommand = new SQLiteCommand(dbConnection);
            var sql = string.Format("select * from Transaction;");
            sqLiteCommand.CommandText = sql;
            var list = new List<string>();
            var transactsList = new List<Transact>();
            var resulr = sqLiteCommand.ExecuteQuery<Transaction>();
            foreach (var c in resulr)
            {
                transact.nameTransact = c.Name;
                transact.sumTransact = c.Sum;
                transactsList.Add(transact);
            }
            return list;
        }

        #endregion



        #region Tebels

        internal class Currency
        {
            [PrimaryKey, AutoIncrement]
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Symbol { get; set; }
            public string Course { get; set; }
            public string Goad { get; set; }
        }

        internal class CurrencyDefault
        {
            [PrimaryKey, AutoIncrement]
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Symbol { get; set; }
            public string Course { get; set; }
            public string Goad { get; set; }
        }

        internal class User
        {
            [PrimaryKey, AutoIncrement]
            public int Id { get; set; }
            public string Name { get; set; }
        }

        internal class CategoryTransaction
        {
            [PrimaryKey, AutoIncrement]
            public int Id { get; set; }
            public string ParentName { get; set; }
            public string ChildName { get; set; }
            public string Type { get; set; }
        }

        internal class Account
        {
            [PrimaryKey, AutoIncrement]
            public int Id { get; set; }
            public string Name { get; set; }
            public string Amount { get; set; }
            public string CurrencyId { get; set; }
        }

        internal class Transaction
        {
            [PrimaryKey, AutoIncrement]
            public int Id { get; set; }
            public string Name { get; set; }
            public string Sum { get; set; }
            public string EndSum { get; set; }
            public string DataTime { get; set; }
            public int UserId { get; set; }
            public int CategoryTransactionId { get; set; }
            public int CurrencyId { get; set; }
        }
        #endregion
    }
}
