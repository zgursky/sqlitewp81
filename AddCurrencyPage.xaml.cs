﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SQLiteWP81
{
    public partial class AddCurrencyPage : PhoneApplicationPage
    {
        List<WorkerDB.CurrencyDefault> currencyDefaults;
        List<string> nameList = new List<string>();
        public AddCurrencyPage()
        {
            _workerDb.AddCurrencyDefault();
            InitializeComponent();
            this.lpkAccount.ItemsSource = ChoiceCurrencyDefault();
        }

        private void GoBack()
        {
            if (this.NavigationService.CanGoBack)
            {

                this.NavigationService.GoBack();

            }

        }

        WorkerDB _workerDb = new WorkerDB();

        private delegate void ChoiseCur();
        private void addCurrency_Click(object sender, RoutedEventArgs e)
        {
            string goad = "";
            var name = textBoxName.Text;
            var description = textBoxDescription.Text;
            var symbol = textBoxSymbol.Text;
            var course = textBoxCourse.Text;
            if (checkGoad != null)
            {
                goad = "1";
            }
            _workerDb.AddCurrency(name, description, symbol, course, goad);
            NavigationService.Navigate(new Uri("/CurrencySettingsPage.xaml", UriKind.Relative));
        }



        public List<string> ChoiceCurrencyDefault()
        {
            currencyDefaults = _workerDb.GetCurrencyDefault();
            foreach (var currencyDefault in currencyDefaults)
            {
                nameList.Add(currencyDefault.Name);
            }
            return nameList;
        }

        private void LpkAccount_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var currencyDefault in currencyDefaults)
            {
                if (this.lpkAccount.SelectedItem == currencyDefault.Name)
                {
                    textBoxName.Text = currencyDefault.Name;
                    textBoxCourse.Text = currencyDefault.Course;
                    textBoxSymbol.Text = currencyDefault.Symbol;
                    textBoxDescription.Text = currencyDefault.Description;
                }
            }
        }

        public void ChoiceCurrencyHelper(string name)
        {

        }

        private void BackKeyPress(object sender, CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/CurrencySettingsPage.xaml", UriKind.Relative));
        }
    }
}