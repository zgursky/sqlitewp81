﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SQLiteWP81
{
    public partial class CurrencySettingsPage : PhoneApplicationPage
    {
        WorkerDB _query = new WorkerDB();
        public CurrencySettingsPage()
        {
            InitializeComponent();
            ShowCurrency();
        }

        private void addCurrency_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddCurrencyPage.xaml", UriKind.Relative));
        }

        public void ShowCurrency()
        {
            List<WorkerDB.Currency> list = _query.ShowCurrency();
            //List<string> liststr = new List<string>();
            List<CurrencyHelper> currencyHelpers = new List<CurrencyHelper>();
            foreach (var currency in list)
            {
                //liststr.Add("Счет: " + account.Name + ".   Баланс: " + account.Amount + ".");
                currencyHelpers.Add(new CurrencyHelper { Course = currency.Course, Name = currency.Name, Description = currency.Description});
            }
            CurrencyListBox.ItemsSource = currencyHelpers;
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Edit");
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var name = sender as MenuItem;
            var str = name.CommandParameter;
            var currencyName = ((CurrencyHelper)(str)).Name;
            _query.DeleteCurrency(currencyName);
            MessageBox.Show("Удален cчет: " + currencyName + ".");
            ShowCurrency();
        }

        private void clear_all_currency_Click(object sender, EventArgs e)
        {
            _query.DeleteAllCurrency();
            MessageBox.Show("Удалены все валюты");
            ShowCurrency();
        }

        private void BackKeyPress(object sender, CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
        }
    }

    public class CurrencyHelper
    {
        public string Name { get; set; }
        public string Course { get; set; }
        public string Description { get; set; }
    }
}