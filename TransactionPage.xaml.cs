﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SQLiteWP81
{
    public partial class TransactionPage : PhoneApplicationPage
    {
        WorkerDB query = new WorkerDB();

        public TransactionPage()
        {
            InitializeComponent();
            this.lpkAccount.ItemsSource = query.GetAccountName();
        }

            //String[] Category = { "Транспорт","Обед",
            //                  "Квартира","Продукты",
            //                  "Кино","Услуги",
            //                  "Мобильный","Пиво"};

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            String _Content = String.Format("\nCountry: {0}", lpkAccount.SelectedItem);
            MessageBox.Show(_Content);
        }

        //private void ResultButton_Click(object sender, RoutedEventArgs e)
        //{
        //    //Showing the Selected Item of ComboBox in a TextBlock  
        //    //Firstly the Selected Item is fetched  
        //    //then it is Type Casted to ComboBoxItem  
        //    //after that Content of the Selected item is fetched   
        //    //by using the Content Property of the ComboBox   
        //    //and then ToString() method is used to return a String  
        //    ResultTextBlock.Text = ((ComboBoxItem)ComboBoxMenu.SelectedItem).Content.ToString();
        //}  
    }
}