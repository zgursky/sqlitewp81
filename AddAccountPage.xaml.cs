﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SQLiteWP81
{
    public partial class AddAccountPage : PhoneApplicationPage
    {
        WorkerDB query = new WorkerDB();
        public AddAccountPage()
        {
            InitializeComponent();
            this.lpkCurrName.ItemsSource = query.GetCurencyNameList();
            textBoxNameAccount.Focus();
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            String _Content = String.Format("\nCurrencyName: {0}", lpkCurrName.SelectedItem);
            MessageBox.Show(_Content);
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            query.SetAccount(textBoxNameAccount.Text, textBoxAmount.Text, lpkCurrName.SelectedItem.ToString());
            MessageBox.Show("Добавлен счет: " + textBoxNameAccount.Text);
            NavigationService.Navigate(new Uri("/AccountsPage.xaml", UriKind.Relative));
        }

        private void BackKeyPress(object sender, CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AccountsPage.xaml", UriKind.Relative));
        }
    }
}